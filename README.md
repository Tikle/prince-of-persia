# Tikle's custom Prince of Persia

## How to run the game

You'll need to have a DOS version of Prince of Persia. The following will replace the game with my custom levels so make sure to make a copy beforehand.

1. Make a copy of the folder of your Prince of Persia game (1990 on DOS, v1.0) called `PRINCE-TIKLE`.
2. Copy every file of this repository inside the folder. This will replace the original game files with my custom files.
3. Use DOSBox to run the game. After launching DOSBox, first mount your folder on the DOSBox C drive by typing `MOUNT C C:\YOUR\PATH\TO\PRINCE-TIKLE\`, then switch to the C drive by typing `C:`, and finally run the game by typing `PRINCE`.

## Credits

This was made using the apoplexy editor : https://apoplexy.github.io/apoplexysite/
